/* Javascript Main File */

_import('https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js');


function onload() {
document.getElementById('container').style.opacity = 1;
document.getElementById('footer').style.display = 'block';
}


// Import others scripts
function _import(url){
	var head = document['head'] || document.getElementsByTagName("head")[0] || document.documentElement;
	var script = document.createElement("script");
	script.type = 'text/javascript';
	script.src = url;
	head.insertBefore(script, head.firstChild);
}


// GOOGLE Analytics 
(function(i, s, o, g, r, a, m) {
i['GoogleAnalyticsObject'] = r;
i[r] = i[r] || function() {
  (i[r].q = i[r].q || []).push(arguments)
}, i[r].l = 1 * new Date();
a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
a.async = 1;
a.src = g;
m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-44799350-1', 'desligare.tk');
ga('send', 'pageview');