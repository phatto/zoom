<?php
/**
 * Controller Main
 *
 * @author Paulo
 */

use Lib\Neos\Phen\View;
use Lib\Book\Db\Db;
use Lib\Book\Config\Config;

// TODO IDEIA: Phen é o Template/Layout e as views serão carregadas como sub-objetos desta
// $vw = new Phen();
// $vw->load('home/inicial', 'main'); //cria um novo objeto View com o nome de 'main'
// $vw->main->value('nome', 'conteudo'); //posso trabalhar com esse novo sub-objeto

// $vw->main->load('home/inicial/menu', 'menu');
// $vw->main->menu->value(); paradoxalmente é possivel ter sub-sub-view ?!?!

class Main{

  function index($v1 = 'nada', $v2 = 'nada'){
    
    $result = Db::query('SELECT * FROM 300k.BOOK WHERE ROUTE = :route', array(':route'=>'/'));
    
    if($result){
        foreach($result as $v){
            echo '<br>ROUTE : '.$v->ROUTE;
            echo '<br>SOLVE : '.$v->SOLVE;
            echo '<br>TITLE : '.$v->TITLE;
            echo '<hr>';
        }
    }
    
   //p(Config::get('DB')->mysql);
    
    $vw = new View();
    $vw ->load('blog/head', 'head')
        ->assign('title', $result[0]->TITLE);
   
    $vw ->load('blog/front', 'body')
        ->assign('content','Este é um teste de PHEN/View.');
    
    $vw ->load('blog/footer', 'footer');
    
    return $vw->render();
  }


}