<?php

/**
 * Description of phen
 *
 * @author Paulo
 */

namespace Lib\Neos\PHEn;

use Lib\Neos\Base;
use Lib\Neos\PHEn\Html;

class View extends Base {

  private $file = '';

  private $views = Array();
  private $values = Array();

  
  function __cosntruct($file = null){
      if($file == null) $this->file = HTML.'default'.HTMLEXT;
      else $this->file = HTML.$file.HTMLEXT;
      
  }
  function assign($var, $value) {
    $this->values[$var] = $value;
    $this->_debug('function assign: ' . $var.' = '.$value);
  }
  
  function load($file, $name) {
    $this->views[$name] = new Html($file);
    $this->_debug('function load: ' . $file.' = '.$name);
    return $this->views[$name];
  }

  function render(){
      $o = '';
      foreach($this->views as $view){
          $o .= $view->render();
      }
      return $o;
  }

}

?>
