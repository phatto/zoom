<?php

/**
 * Description of phen
 *
 * @author Paulo
 */

namespace Lib\Neos\PHEn;
use Lib\Neos\Base;
use Lib\Neos\PHEn\View;

class Html extends Base {

  private $file = '';
  private $values = Array();

  function __construct($file){
      $this->file = HTML.$file.HTMLEXT;
  }

  function assign($var, $value) {
    $this->values[$var] = $value;
    $this->_debug('function ASSIGN:'."\n\t\tvar:\t".$var."\n\t\tvalue:\t".$value);
  }

  function render(){
      return file_get_contents($this->file);
  }

}

?>
