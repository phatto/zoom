<?php
/**
 * Description of config
 *
 * @author Paulo
 */

namespace Lib\Book;

class Config {

  static $file = '';
  static $config = array();

  function __construct($file = null){
    if($file != null) static::$file = $file;
    if(file_exists(static::$file)) static::$config = json_decode(file_get_contents(static::$file));
    else trigger_error ('Config file "'.static::$file.'" not exists', E_USER_ERROR);
  }

  static function get($it = null){
    if($it == null) return static::$config;
    if(isset(static::$config->$it))
      return static::$config->$it;
    trigger_error('Config item "'.$it.'" not found', E_USER_NOTICE);
  }

  static function set($it, $value){
    if(isset(static::$config[$it])) return static::$config[$it] = $value;
    trigger_error('Config item "'.$it.'" not found', E_USER_NOTICE);
  }

  static function setFile($file){
    static::$file = $file;
  }

  static function load($file = null){
    if($file != null) static::$file = $file;
    if(file_exists(static::$file)) static::$config = json_decode(file_get_contents(static::$file));
    else trigger_error ('Config file "'.static::$file.'" not exists', E_USER_ERROR);
  }

  static function save($file = null){
    if($file != null) static::$file = $file;
    return file_put_contents(static::$file, json_encode(static::$config));
  }




}