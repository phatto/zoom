<?php
/**
 * Description of result
 *
 * @author Paulo
 */

namespace Lib\Book\DB;
use Lib\Book\Config\Config;
use PDO;

class Result {
    
    private $__sql = '';
    private $__bind = '';
    
    function __construct($sql, $bind){
        $this->__sql = $sql;
        $this->__bind = $bind;
    }
}
