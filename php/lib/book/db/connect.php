<?php
/**
 * Description of connect
 *
 * @author Paulo
 */

namespace Lib\Book\DB;

class Connect {

	static $conn = Array();

	final public static function hdl($alias = null){
      $cfg = \_config::get('DB');
      if(!isset($cfg->$alias)) trigger_error ('Alias "'.$alias.'" DB not configured', E_USER_ERROR);
      $cfg = $cfg->$alias;

      //check if handler exists
      if(isset(static::$conn[$alias]) && is_object(static::$conn[$alias])) return static::$conn[$alias];
      return static::$conn[$alias] = new \PDO($cfg->driver.':host='.$cfg->host, $cfg->user, $cfg->password);
	}
}