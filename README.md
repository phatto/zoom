This project has been aborted - see http://github.com/pedra to find similar production.
==
***


zoOm!
==

[TOC]

----------
Imagine um CMS escrito por brasileiros usando as mais modernas técnicas de desenvolvimento em PHP, CSS, HTML e JavaScript e que você pode alterar, adicionar recursos, copiar e criar sua própria versão e ainda assim ser completamente compatível entre si!

![x][2]  [***Esse é o zoOm!***][3]


Participe
----------

Se você estava procurando um excelente projeto para se dedicar, mostrar seus talentos e **enriquecer seu currículo** considere o **zoOm!**.

Fale diretamente com o autor do projeto:

- skype: "neosphp"
- email: prbr@ymail.com
- tel.:  +55 21 3630-5920


----------
Colaboradores
----------
>![foto][1] Paulo Rocha | http://colabore.co.vu/run | prbr@ymail.com | +55 21 3630-5920

>![avatar][4] Você?!





  [1]: https://lh3.googleusercontent.com/-5RtLRX4XO1c/UiuE541YlPI/AAAAAAAABAg/8GWgw_dbpYc/s50/rocha.png "Rocha"
  [2]: http://www.cap.org.uk/Advertising-Codes/~/media/Images/CAP/Section/CAP%20code/Non%20broadcast/download.ashx
  [3]: http://colabore.co.vu/run
  [4]: https://lh5.googleusercontent.com/-isenTiOkOgQ/UnK2VF3GDtI/AAAAAAAABBk/W2kr7bFkIVA/s50/daniel_miessler_avatar+%25281%2529.jpg "desconhecido"